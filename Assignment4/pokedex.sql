create database pokedex;

create table pokemon (
  num int,
  name text,
  type text,
  hp int,
  atk int,
  def int,
  satk int,
  sdef int,
  spd int,
  total int
);

Insert into pokemon values(387, "Turtwig", "Grass", 55, 68, 64, 45, 55, 31, 318);
Insert into pokemon values(388, "Grotle", "Grass", 75, 89, 85, 55, 65, 36, 405); 
Insert into pokemon values(389, "Torterra", "Grass""Ground", 95, 109, 105, 75, 85, 56, 525);
Insert into pokemon values(390, "Chimchar", "Fire", 44, 58, 44, 58, 44, 61, 309);
Insert into pokemon values(391, "Monferno", "Fire""Fighting", 64, 78, 52, 78, 52, 81, 405);
Insert into pokemon values(392, "Infernape", "Fire""Fighting", 76, 104, 71, 104, 71, 108, 534);
Insert into pokemon values(393, "Piplup", "Water", 53, 51, 53, 61, 56, 40, 314);
Insert into pokemon values(394, "Prinplup", "Water", 64, 66, 68, 81, 76, 50, 405);
Insert into pokemon values(395, "Empoleon", "Water""Steel", 84, 86, 88, 111, 101, 60, 530);
Insert into pokemon values(396, "Starly", "Normal""Flying", 40, 55, 30, 30, 30, 60, 245);
Insert into pokemon values(397, "Staravia", "Normal""Flying", 55, 75, 50, 40, 40, 80, 340);
Insert into pokemon values(398, "Staraptor", "Normal""Flying", 85, 120, 70, 50, 60, 100, 485);
Insert into pokemon values(399, "Bidoof", "Normal", 59, 45, 40, 35, 40, 31, 250);
Insert into pokemon values(400, "Bibarel", "Normal""Water", 79, 85, 60, 55, 60, 71, 410);
Insert into pokemon values(401, "Kricketot", "Bug", 37, 25, 41, 25, 41, 25, 194);
Insert into pokemon values(402, "Kricketune", "Bug", 77, 85, 51, 55, 51, 65, 384);
Insert into pokemon values(403, "Shinx", "Electric", 45, 65, 34, 40, 34, 45, 263);
Insert into pokemon values(404, "Luxio", "Electric", 60, 85, 49, 60, 49, 60, 363);
Insert into pokemon values(405, "Luxray", "Electric", 80, 120, 79, 95, 79, 70, 523);
Insert into pokemon values(406, "Budew", "Grass""Poison", 40, 30, 35, 50, 70, 55, 280);
Insert into pokemon values(407, "Roserade", "Grass""Poison", 60, 70, 65, 125, 105, 90, 515);
Insert into pokemon values(408, "Cranidos", "Rock", 67, 125, 40, 30, 30, 58, 350);
Insert into pokemon values(410, "Shieldon", "Rock""Steel", 30, 42, 118, 42, 88, 30, 350);
Insert into pokemon values(411, "Bastiodon", "Rock""Steel", 60, 52, 168, 47, 138, 30, 495);
Insert into pokemon values(412, "Burmy", "Bug", 40, 29, 45, 29, 45, 36, 224);
Insert into pokemon values(413, "Wormadam", "Bug""Grass", 60, 59, 85, 79, 105, 36, 424);

/*
"41401-sandy-cloak"
"https://media-cerulean.cursecdn.com/avatars/322/60/725.png.png"
Wormadam 
 (Sandy Cloak) 
 
Insert into pokemon values("Bug""Ground", 60, 79, 105, 59, 85, 36, 424);  413, 
-
, 
 
"41402-trash-cloak"
"https://media-cerulean.cursecdn.com/avatars/322/63/726.png.png"
Wormadam 
 (Trash Cloak) 
 
Insert into pokemon values("Bug""Steel", 60, 69, 95, 69, 95, 36, 424);  414, 

*/


Insert into pokemon values(414, "Mothim", "Bug""Flying", 70, 94, 50, 94, 50, 66, 424);
Insert into pokemon values(415, "Combee", "Bug""Flying", 30, 30, 42, 30, 42, 70, 244);
Insert into pokemon values(416, "Vespiquen", "Bug""Flying", 70, 80, 102, 80, 102, 40, 474);
Insert into pokemon values(417, "Pachirisu", "Electric", 60, 45, 70, 45, 90, 95, 405);
Insert into pokemon values(418, "Buizel", "Water", 55, 65, 35, 60, 30, 85, 330);
Insert into pokemon values(419, "Floatzel", "Water", 85, 105, 55, 85, 50, 115, 495);
Insert into pokemon values(420, "Cherubi", "Grass", 45, 35, 45, 62, 53, 35, 275);
Insert into pokemon values(421, "Cherrim", "Grass", 70, 60, 70, 87, 78, 85, 450);


/*
"42201-sunshine-form"
"https://media-cerulean.cursecdn.com/avatars/322/90/737.png.png"
Cherrim 
 (Sunshine Form) 
 
Insert into pokemon values("Grass", 70, 60, 70, 87, 78, 85, 450);  422, 
*/

Insert into pokemon values(422, "Shellos", "Water", 76, 48, 48, 57, 62, 34, 325);
Insert into pokemon values(423, "Gastrodon", "Water""Ground", 111, 83, 68, 92, 82, 39, 475);
Insert into pokemon values(424, "Ambipom", "Normal", 75, 100, 66, 60, 66, 115, 482);
Insert into pokemon values(425, "Drifloon", "Ghost""Flying", 90, 50, 34, 60, 44, 70, 348);
Insert into pokemon values(426, "Drifblim", "Ghost""Flying", 150, 80, 44, 90, 54, 80, 498);
Insert into pokemon values(427, "Buneary", "Normal", 55, 66, 44, 44, 56, 85, 350);
Insert into pokemon values(428, "Lopunny", "Normal", 65, 76, 84, 54, 96, 105, 480);



/*
"42901-mega-lopunny"
"https://media-cerulean.cursecdn.com/avatars/322/120/823.png.png"
Mega Lopunny 
 
Insert into pokemon values("Normal""Fighting", 65, 136, 94, 54, 96, 135, 580);  429, 
-
, 
*/



Insert into pokemon values(429, "Mismagius", "Ghost", 60, 60, 60, 105, 105, 105, 495);
Insert into pokemon values(430, "Honchkrow", "Dark""Flying", 100, 125, 52, 105, 52, 71, 505);
Insert into pokemon values(431, "Glameow", "Normal", 49, 55, 42, 42, 37, 85, 310);
Insert into pokemon values(432, "Purugly", "Normal", 71, 82, 64, 64, 59, 112, 452);
Insert into pokemon values(433, "Chingling", "Psychic", 45, 30, 50, 65, 50, 45, 285);
Insert into pokemon values(434, "Stunky", "Poison""Dark", 63, 63, 47, 41, 41, 74, 329);
Insert into pokemon values(435, "Skuntank", "Poison""Dark", 103, 93, 67, 71, 61, 84, 479);
Insert into pokemon values(436, "Bronzor", "Steel""Psychic", 57, 24, 86, 24, 86, 23, 300);
Insert into pokemon values(437, "Bronzong", "Steel""Psychic", 67, 89, 116, 79, 116, 33, 500);
Insert into pokemon values(439, "Mime Jr.", "Psychic""Fairy", 20, 25, 45, 70, 90, 60, 310);
Insert into pokemon values(440, "Happiny", "Normal", 100, 5, 5, 15, 65, 30, 220);
Insert into pokemon values(441, "Chatot", "Normal""Flying", 76, 65, 45, 92, 42, 91, 411);
Insert into pokemon values(442, "Spiritomb", "Ghost""Dark", 50, 92, 108, 92, 108, 35, 485);
Insert into pokemon values(443, "Gible", "Dragon""Ground", 58, 70, 45, 40, 45, 42, 300);
Insert into pokemon values(444, "Gabite", "Dragon""Ground", 68, 90, 65, 50, 55, 82, 410);
Insert into pokemon values(445, "Garchomp", "Dragon""Ground", 108, 130, 95, 80, 85, 102, 600);




/*
Insert into pokemon values("10058-mega-garchomp"
"https://media-cerulean.cursecdn.com/avatars/322/174/785.png.png"
Mega Garchomp 
 
Insert into pokemon values("Dragon""Ground", 108, 170, 115, 120, 95, 92, 700);  446, 
-
*/



Insert into pokemon values(446, "Munchlax", "Normal", 135, 85, 40, 40, 85, 5, 390);
Insert into pokemon values(447, "Riolu", "Fighting", 40, 70, 40, 35, 40, 60, 285);
Insert into pokemon values(448, "Lucario", "Fighting""Steel", 70, 110, 70, 115, 70, 90, 525);



/*
 
Insert into pokemon values("100076-mega-lucario"
"https://media-cerulean.cursecdn.com/avatars/322/186/774.png.png"
Mega Lucario 
 
Insert into pokemon values("Fighting""Steel", 70, 145, 88, 140, 70, 112, 625);  449, 
-
, 

*/



Insert into pokemon values(449, "Hippopotas", "Ground", 68, 72, 78, 38, 42, 32, 330);
Insert into pokemon values(450, "Hippowdon", "Ground", 108, 112, 118, 68, 72, 47, 525);
Insert into pokemon values(451, "Skorupi", "Poison""Bug", 40, 50, 90, 30, 55, 65, 330);
Insert into pokemon values(452, "Drapion", "Poison""Dark", 70, 90, 110, 60, 75, 95, 500);
Insert into pokemon values(453, "Croagunk", "Poison""Fighting", 48, 61, 40, 61, 40, 50, 300);
Insert into pokemon values(454, "Toxicroak", "Poison""Fighting", 83, 106, 65, 86, 65, 85, 490);
Insert into pokemon values(455, "Carnivine", "Grass", 74, 100, 72, 90, 72, 46, 454);
Insert into pokemon values(456, "Finneon", "Water", 49, 49, 56, 49, 61, 66, 330);
Insert into pokemon values(457, "Lumineon", "Water", 69, 69, 76, 69, 86, 91, 460);
Insert into pokemon values(458, "Mantyke", "Water""Flying", 45, 20, 50, 60, 120, 50, 345);
Insert into pokemon values(459, "Snover", "Grass""Ice", 60, 62, 50, 62, 60, 40, 334);
Insert into pokemon values(460, "Abomasnow", "Grass""Ice", 90, 92, 75, 92, 85, 60, 494);  460, 
-
, 


/*
 
Insert into pokemon values("10060-mega-abomasnow"
"https://media-cerulean.cursecdn.com/avatars/322/225/775.png.png"
Mega Abomasnow 
 
Insert into pokemon values("Grass""Ice", 90, 132, 105, 132, 105, 30, 594);  461, 


*/



Insert into pokemon values(461, "Weavile", "Dark""Ice", 70, 120, 65, 45, 85, 125, 510);
Insert into pokemon values(462, "Magnezone", "Electric""Steel", 70, 70, 115, 130, 90, 60, 535);
Insert into pokemon values(463, "Lickilicky", "Normal", 110, 85, 95, 80, 95, 50, 515);
Insert into pokemon values(464, "Rhyperior", "Ground""Rock", 115, 140, 130, 55, 55, 40, 535);
Insert into pokemon values(465, "Tangrowth", "Grass", 100, 100, 125, 110, 50, 50, 535);
Insert into pokemon values(466, "Electivire", "Electric", 75, 123, 67, 95, 85, 95, 540);
Insert into pokemon values(467, "Magmortar", "Fire", 75, 95, 67, 125, 95, 83, 540);
Insert into pokemon values(468, "Togekiss", "Fairy""Flying", 85, 50, 95, 120, 115, 80, 545);
Insert into pokemon values(469, "Yanmega", "Bug""Flying", 86, 76, 86, 116, 56, 95, 515);
Insert into pokemon values(470, "Leafeon", "Grass", 65, 110, 130, 60, 65, 95, 525);
Insert into pokemon values(471, "Glaceon", "Ice", 65, 60, 110, 130, 95, 65, 525);
Insert into pokemon values(472, "Gliscor", "Ground""Flying", 75, 95, 125, 45, 75, 95, 510);
Insert into pokemon values(473, "Mamoswine", "Ice""Ground", 110, 130, 80, 70, 60, 80, 530);
Insert into pokemon values(474, "Porygon-Z", "Normal", 85, 80, 70, 135, 75, 90, 535);
Insert into pokemon values(475, "Gallade", "Psychic""Fighting", 68, 125, 65, 65, 115, 80, 518);



/*

"47601-mega-gallade"
"https://media-cerulean.cursecdn.com/avatars/322/273/803.png.png"
Mega Gallade 
 
Insert into pokemon values("Psychic""Fighting", 68, 165, 95, 65, 115, 110, 618);  476, 62

*/




Insert into pokemon values(476, "Probopass", "Rock""Steel", 60, 55, 145, 75, 150, 40, 525);
Insert into pokemon values(477, "Dusknoir", "Ghost", 45, 100, 135, 65, 135, 45, 525);
Insert into pokemon values(478, "Froslass", "Ice""Ghost", 70, 80, 70, 80, 70, 110, 480);
Insert into pokemon values(479, "Rotom", "Electric""Ghost", 50, 50, 77, 95, 77, 91, 440);  479, 


/*



"48001-heat-rotom"
"https://media-cerulean.cursecdn.com/avatars/322/288/729.png.png"
Rotom 
 (Heat Rotom) 
 
Insert into pokemon values("Electric""Fire", 50, 65, 107, 105, 107, 86, 520);  479, 
-
, 
 
"48002-wash-rotom"
"https://media-cerulean.cursecdn.com/avatars/322/291/730.png.png"
Rotom 
 (Wash Rotom) 
 
Insert into pokemon values("Electric""Water", 50, 65, 107, 105, 107, 86, 520);  479, 
-
, 
 
"48003-frost-rotom"
"https://media-cerulean.cursecdn.com/avatars/322/294/731.png.png"
Rotom 
 (Frost Rotom) 
 
Insert into pokemon values("Electric""Ice", 50, 65, 107, 105, 107, 86, 520);  479, 
-
, 
 
"48004-fan-rotom"
"https://media-cerulean.cursecdn.com/avatars/322/297/732.png.png"
Rotom 
 (Fan Rotom) 
 
Insert into pokemon values("Electric""Flying", 50, 65, 107, 105, 107, 86, 520);  479, 
-
, 
 
"48005-mow-rotom"
"https://media-cerulean.cursecdn.com/avatars/322/300/733.png.png"
Rotom 
 (Mow Rotom) 
 
Insert into pokemon values("Electric""Grass", 50, 65, 107, 105, 107, 86, 520);


*/







Insert into pokemon values(480, "Uxie", "Psychic", 75, 75, 130, 75, 130, 95, 580);
Insert into pokemon values(481, "Mesprit", "Psychic", 80, 105, 105, 105, 105, 80, 580);
Insert into pokemon values(482, "Azelf", "Psychic", 75, 125, 70, 125, 70, 115, 580);
Insert into pokemon values(483, "Dialga", "Steel""Dragon", 100, 120, 120, 150, 100, 90, 680);
Insert into pokemon values(484, "Palkia", "Water""Dragon", 90, 120, 100, 150, 120, 100, 680);
Insert into pokemon values(485, "Heatran", "Fire""Steel", 91, 90, 106, 130, 106, 77, 600);
Insert into pokemon values(486, "Regigigas", "Normal", 110, 160, 110, 80, 110, 100, 670);
Insert into pokemon values(487, "Giratina", "Ghost""Dragon", 150, 100, 120, 100, 120, 90, 680);




/*

"48801-origin-forme"
"https://media-cerulean.cursecdn.com/avatars/322/327/728.png.png"
Giratina 
 (Origin Forme) 
 
Insert into pokemon values("Ghost""Dragon", 150, 120, 100, 120, 100, 90, 680);  488, 


*/



Insert into pokemon values(488, "Cresselia", "Psychic", 120, 70, 120, 75, 130, 85, 600);
Insert into pokemon values(489, "Phione", "Water", 80, 80, 80, 80, 80, 80, 480);
Insert into pokemon values(490, "Manaphy", "Water", 100, 100, 100, 100, 100, 100, 600);
Insert into pokemon values(491, "Darkrai", "Dark", 70, 90, 90, 135, 90, 125, 600);
Insert into pokemon values(492, "Shaymin", "Grass", 100, 100, 100, 100, 100, 100, 600);



/*

"49301-sky-forme"
"https://media-cerulean.cursecdn.com/avatars/322/345/727.png.png"
Shaymin 
 (Sky Forme) 
 
Insert into pokemon values("Grass""Flying", 100, 103, 75, 120, 75, 127, 600);  493, 

*/




Insert into pokemon values(493, "Arceus", "Normal", 120, 120, 120, 120, 120, 120, 720);
Insert into pokemon values(494, "Victini", "Psychic""Fire", 100, 100, 100, 100, 100, 100, 600);
Insert into pokemon values(495, "Snivy", "Grass", 45, 45, 55, 45, 55, 63, 308);
Insert into pokemon values(496, "Servine", "Grass", 60, 60, 75, 60, 75, 83, 413);
Insert into pokemon values(497, "Serperior", "Grass", 75, 75, 95, 75, 95, 113, 528);
Insert into pokemon values(498, "Tepig", "Fire", 65, 63, 45, 45, 45, 45, 308);
Insert into pokemon values(499, "Pignite", "Fire""Fighting", 90, 93, 55, 70, 55, 55, 418);
Insert into pokemon values(500, "Emboar", "Fire""Fighting", 110, 123, 65, 100, 65, 65, 528);
Insert into pokemon values(501, "Oshawott", "Water", 55, 55, 45, 63, 45, 45, 308);
Insert into pokemon values(502, "Dewott", "Water", 75, 75, 60, 83, 60, 60, 413);
Insert into pokemon values(503, "Samurott", "Water", 95, 100, 85, 108, 70, 70, 528);
Insert into pokemon values(504, "Patrat", "Normal", 45, 55, 39, 35, 39, 42, 255);
Insert into pokemon values(505, "Watchdog", "Normal", 60, 85, 69, 60, 69, 77, 420);
Insert into pokemon values(506, "Lillipup", "Normal", 45, 60, 45, 25, 45, 55, 275);
Insert into pokemon values(507, "Herdier", "Normal", 65, 80, 65, 35, 65, 60, 370);
Insert into pokemon values(508, "Stoutland", "Normal", 85, 110, 90, 45, 90, 80, 500);
Insert into pokemon values(509, "Purrloin", "Dark", 41, 50, 37, 50, 37, 66, 281);
Insert into pokemon values(510, "Liepard", "Dark", 64, 88, 50, 88, 50, 106, 446);
Insert into pokemon values(511, "Pansage", "Grass", 50, 53, 48, 53, 48, 64, 316);
Insert into pokemon values(512, "Simisage", "Grass", 75, 98, 63, 98, 63, 101, 498);
Insert into pokemon values(513, "Pansear", "Fire", 50, 53, 48, 53, 48, 64, 316);
Insert into pokemon values(514, "Simisear", "Fire", 75, 98, 63, 98, 63, 101, 498);
Insert into pokemon values(515, "Panpour", "Water", 50, 53, 48, 53, 48, 64, 316);
Insert into pokemon values(516, "Simipour", "Water", 75, 98, 63, 98, 63, 101, 498);
Insert into pokemon values(517, "Munna", "Psychic", 76, 25, 45, 67, 55, 24, 292);
Insert into pokemon values(518, "Musharna", "Psychic", 116, 55, 85, 107, 95, 29, 487);
Insert into pokemon values(519, "Pidove", "Normal""Flying", 50, 55, 50, 36, 30, 43, 264);
Insert into pokemon values(520, "Tranquill", "Normal""Flying", 62, 77, 62, 50, 42, 65, 358);
Insert into pokemon values(521, "Unfezant", "Normal""Flying", 80, 115, 80, 65, 55, 93, 488);
Insert into pokemon values(522, "Blitzle", "Electric", 45, 60, 32, 50, 32, 76, 295);
Insert into pokemon values(523, "Zebstrika", "Electric", 75, 100, 63, 80, 63, 116, 497);
Insert into pokemon values(524, "Roggenrola", "Rock", 55, 75, 85, 25, 25, 15, 280);
Insert into pokemon values(525, "Boldore", "Rock", 70, 105, 105, 50, 40, 20, 390);
Insert into pokemon values(526, "Gigalith", "Rock", 85, 135, 130, 60, 80, 25, 515);
Insert into pokemon values(527, "Woobat", "Psychic""Flying", 55, 45, 43, 55, 43, 72, 313);
Insert into pokemon values(528, "Swoobat", "Psychic""Flying", 67, 57, 55, 77, 55, 114, 425);
Insert into pokemon values(529, "Drilbur", "Ground", 60, 85, 40, 30, 45, 68, 328);
Insert into pokemon values(530, "Excadrill", "Ground""Steel", 110, 135, 60, 50, 65, 88, 508);
Insert into pokemon values(531, "Audino", "Normal", 103, 60, 86, 60, 86, 50, 445);




/*

"53201-mega-audino"
"https://media-cerulean.cursecdn.com/avatars/322/516/804.png.png"
Mega Audino 
 
Insert into pokemon values("Normal""Fairy", 103, 60, 126, 80, 126, 50, 545);  532, 


*/




Insert into pokemon values(532, "Timburr", "Fighting", 75, 80, 55, 25, 35, 35, 305);
Insert into pokemon values(533, "Gurdurr", "Fighting", 85, 105, 85, 40, 50, 40, 405);
Insert into pokemon values(534, "Conkeldurr", "Fighting", 105, 140, 95, 55, 65, 45, 505);
Insert into pokemon values(535, "Tympole", "Water", 50, 50, 40, 50, 40, 64, 294);
Insert into pokemon values(536, "Palpitoad", "Water""Ground", 75, 65, 55, 65, 55, 69, 384);
Insert into pokemon values(537, "Seismitoad", "Water""Ground", 105, 95, 75, 85, 75, 74, 509);
Insert into pokemon values(538, "Throh", "Fighting", 120, 100, 85, 30, 85, 45, 465);
Insert into pokemon values(539, "Sawk", "Fighting", 75, 125, 75, 30, 75, 85, 465);
Insert into pokemon values(540, "Sewaddle", "Bug""Grass", 45, 53, 70, 40, 60, 42, 310);
Insert into pokemon values(541, "Swadloon", "Bug""Grass", 55, 63, 90, 50, 80, 42, 380);
Insert into pokemon values(542, "Leavanny", "Bug""Grass", 75, 103, 80, 70, 80, 92, 500);
Insert into pokemon values(543, "Venipede", "Bug""Poison", 30, 45, 59, 30, 39, 57, 260);
Insert into pokemon values(544, "Whirlipede", "Bug""Poison", 40, 55, 99, 40, 79, 47, 360);
Insert into pokemon values(545, "Scolipede", "Bug""Poison", 60, 100, 89, 55, 69, 112, 485);
Insert into pokemon values(546, "Cottonee", "Grass""Fairy", 40, 27, 60, 37, 50, 66, 280);
Insert into pokemon values(547, "Whimsicott", "Grass""Fairy", 60, 67, 85, 77, 75, 116, 480);
Insert into pokemon values(548, "Petilil", "Grass", 45, 35, 50, 70, 50, 30, 280);
Insert into pokemon values(549, "Lilligant", "Grass", 70, 60, 75, 110, 75, 90, 480);
Insert into pokemon values(550, "Basculin", "Water", 70, 92, 65, 80, 55, 98, 460);





/*


"55101-blue-striped-form"
"https://media-cerulean.cursecdn.com/avatars/322/576/738.png.png"
Basculin 
 (Blue-Striped Form) 
 
Insert into pokemon values("Water", 70, 92, 65, 80, 55, 98, 460);  551, 


*/

 



Insert into pokemon values(551, "Sandile", "Ground""Dark", 50, 72, 35, 35, 35, 65, 292);
Insert into pokemon values(552, "Krokorok", "Ground""Dark", 60, 82, 45, 45, 45, 74, 351);
Insert into pokemon values(553, "Krookodile", "Ground""Dark", 95, 117, 80, 65, 70, 92, 519);
Insert into pokemon values(554, "Darumaka", "Fire", 70, 90, 45, 15, 45, 50, 315);
Insert into pokemon values(555, "Darmanitan", "Fire", 105, 140, 55, 30, 55, 95, 480);





/*

"55601-zen-mode"
"https://media-cerulean.cursecdn.com/avatars/322/594/739.png.png"
Darmanitan 
 (Zen Mode) 
 
Insert into pokemon values("Fire""Psychic", 105, 30, 105, 140, 105, 55, 540);  556, 

*/






Insert into pokemon values(556, "Maractus", "Grass", 75, 86, 67, 106, 67, 60, 461);
Insert into pokemon values(557, "Dwebble", "Bug""Rock", 50, 65, 85, 35, 35, 55, 325);
Insert into pokemon values(558, "Crustle", "Bug""Rock", 70, 95, 125, 65, 75, 45, 475);
Insert into pokemon values(559, "Scraggy", "Dark""Fighting", 50, 75, 70, 35, 70, 48, 348);
Insert into pokemon values(560, "Scrafty", "Dark""Fighting", 65, 90, 115, 45, 115, 58, 488);
Insert into pokemon values(561, "Sigilyph", "Psychic""Flying", 72, 58, 80, 103, 80, 97, 490);
Insert into pokemon values(562, "Yamask", "Ghost", 38, 30, 85, 55, 65, 30, 303);
Insert into pokemon values(563, "Cofagrigus", "Ghost", 58, 50, 145, 95, 105, 30, 483);
Insert into pokemon values(564, "Tirtouga", "Water""Rock", 54, 78, 103, 53, 45, 22, 355);
Insert into pokemon values(565, "Carracosta", "Water""Rock", 74, 108, 133, 83, 65, 32, 495);
Insert into pokemon values(566, "Archen", "Rock""Flying", 55, 112, 45, 74, 45, 70, 401);
Insert into pokemon values(567, "Archeops", "Rock""Flying", 75, 140, 65, 112, 65, 110, 567);
Insert into pokemon values(568, "Trubbish", "Poison", 50, 50, 62, 40, 62, 65, 329);
Insert into pokemon values(569, "Garbodor", "Poison", 80, 95, 82, 60, 82, 75, 474);
Insert into pokemon values(570, "Zorua", "Dark", 40, 65, 40, 80, 40, 65, 330);
Insert into pokemon values(571, "Zoroark", "Dark", 60, 105, 60, 120, 60, 105, 510);
Insert into pokemon values(572, "Minccino", "Normal", 55, 50, 40, 40, 40, 75, 300);
Insert into pokemon values(573, "Cinccino", "Normal", 75, 95, 60, 65, 60, 115, 470);
Insert into pokemon values(574, "Gothita", "Psychic", 45, 30, 50, 55, 65, 45, 290);
Insert into pokemon values(575, "Gothorita", "Psychic", 60, 45, 70, 75, 85, 55, 390);
Insert into pokemon values(576, "Gothitelle", "Psychic", 70, 55, 95, 95, 110, 65, 490);
Insert into pokemon values(577, "Solosis", "Psychic", 45, 30, 40, 105, 50, 20, 290);
Insert into pokemon values(578, "Duosion", "Psychic", 65, 40, 50, 125, 60, 30, 370);
Insert into pokemon values(579, "Reuniclus", "Psychic", 110, 65, 75, 125, 85, 30, 490);
Insert into pokemon values(580, "Ducklett", "Water""Flying", 62, 44, 50, 44, 50, 55, 305);
Insert into pokemon values(581, "Swanna", "Water""Flying", 75, 87, 63, 87, 63, 98, 473);
Insert into pokemon values(582, "Vanillite", "Ice", 36, 50, 50, 65, 60, 44, 305);
Insert into pokemon values(583, "Vanillish", "Ice", 51, 65, 65, 80, 75, 59, 395);
Insert into pokemon values(584, "Vanilluxe", "Ice", 71, 95, 85, 110, 95, 79, 535);
Insert into pokemon values(585, "Deerling", "Normal""Grass", 60, 60, 50, 40, 50, 75, 335);
Insert into pokemon values(586, "Sawsbuck", "Normal""Grass", 80, 100, 70, 60, 70, 95, 475);
Insert into pokemon values(587, "Emolga", "Electric""Flying", 55, 75, 60, 75, 60, 103, 428);
Insert into pokemon values(588, "Karrablast", "Bug", 50, 75, 45, 40, 45, 60, 315);
Insert into pokemon values(589, "Escavalier", "Bug""Steel", 70, 135, 105, 60, 105, 20, 495);
Insert into pokemon values(590, "Foongus", "Grass""Poison", 69, 55, 45, 55, 55, 15, 294);
Insert into pokemon values(591, "Amoonguss", "Grass""Poison", 114, 85, 70, 85, 80, 30, 464);
Insert into pokemon values(592, "Frillish", "Water""Ghost", 55, 40, 50, 65, 85, 40, 335);
Insert into pokemon values(593, "Jellicent", "Water""Ghost", 100, 60, 70, 85, 105, 60, 480);
Insert into pokemon values(594, "Alomomola", "Water", 165, 75, 80, 40, 45, 65, 470);
Insert into pokemon values(595, "Joltik", "Bug""Electric", 50, 47, 50, 57, 50, 65, 319);
Insert into pokemon values(596, "Galvantula", "Bug""Electric", 70, 77, 60, 97, 60, 108, 472);
Insert into pokemon values(597, "Ferroseed", "Grass""Steel", 44, 50, 91, 24, 86, 10, 305);
Insert into pokemon values(598, "Ferrothorn", "Grass""Steel", 74, 94, 131, 54, 116, 20, 489);
Insert into pokemon values(599, "Klink", "Steel", 40, 55, 70, 45, 60, 30, 300);
Insert into pokemon values(600, "Klang", "Steel", 60, 80, 95, 70, 85, 50, 440);
Insert into pokemon values(601, "Klinklang", "Steel", 60, 100, 115, 70, 85, 90, 520);
Insert into pokemon values(602, "Tynamo", "Electric", 35, 55, 40, 45, 40, 60, 275);
Insert into pokemon values(603, "Eelektrik", "Electric", 65, 85, 70, 75, 70, 40, 405);
Insert into pokemon values(604, "Eelektross", "Electric", 85, 115, 80, 105, 80, 50, 515);
Insert into pokemon values(605, "Elgyem", "Psychic", 55, 55, 55, 85, 55, 30, 335);
Insert into pokemon values(606, "Beheeyem", "Psychic", 75, 75, 75, 125, 95, 40, 485);
Insert into pokemon values(607, "Litwick", "Ghost""Fire", 50, 30, 55, 65, 55, 20, 275);
Insert into pokemon values(608, "Lampent", "Ghost""Fire", 60, 40, 60, 95, 60, 55, 370);
Insert into pokemon values(609, "Chandelure", "Ghost""Fire", 60, 55, 90, 145, 90, 80, 520);
Insert into pokemon values(610, "Axew", "Dragon", 46, 87, 60, 30, 40, 57, 320);
Insert into pokemon values(611, "Fraxure", "Dragon", 66, 117, 70, 40, 50, 67, 410);
Insert into pokemon values(612, "Haxorus", "Dragon", 76, 147, 90, 60, 70, 97, 540);
Insert into pokemon values(613, "Cubchoo", "Ice", 55, 70, 40, 60, 40, 40, 305);
Insert into pokemon values(614, "Beartic", "Ice", 95, 110, 80, 70, 80, 50, 485);
Insert into pokemon values(615, "Cryogonal", "Ice", 70, 50, 30, 95, 135, 105, 485);
Insert into pokemon values(616, "Shelmet", "Bug", 50, 40, 85, 40, 65, 25, 305);
Insert into pokemon values(617, "Accelgor", "Bug", 80, 70, 40, 100, 60, 145, 495);
Insert into pokemon values(618, "Stunfisk", "Ground""Electric", 109, 66, 84, 81, 99, 32, 471);
Insert into pokemon values(619, "Mienfoo", "Fighting", 45, 85, 50, 55, 50, 65, 350);
Insert into pokemon values(620, "Mienshao", "Fighting", 65, 125, 60, 95, 60, 105, 510);
Insert into pokemon values(621, "Druddigon", "Dragon", 77, 120, 90, 60, 90, 48, 485);
Insert into pokemon values(622, "Golett", "Ground""Ghost", 59, 74, 50, 35, 50, 35, 303);
Insert into pokemon values(623, "Golurk", "Ground""Ghost", 89, 124, 80, 55, 80, 55, 483);
Insert into pokemon values(624, "Pawniard", "Dark""Steel", 45, 85, 70, 40, 40, 60, 340);
Insert into pokemon values(625, "Bisharp", "Dark""Steel", 65, 125, 100, 60, 70, 70, 490);
Insert into pokemon values(626, "Bouffalant", "Normal", 95, 110, 95, 40, 95, 55, 490);
Insert into pokemon values(627, "Rufflet", "Normal""Flying", 70, 83, 50, 37, 50, 60, 350);